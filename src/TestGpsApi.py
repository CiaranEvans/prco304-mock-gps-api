import MockGpsApi
import json
from unittest import TestCase, main


class TestGpsApi(TestCase):

    expected_no_track_message = 'No route available for: '
    expected_correct_coordinates_call_one = [-4.14059579372406, 50.375312830644, 0]
    expected_correct_coordinates_call_two = [-4.140568971633911, 50.37538809675052, 1]
    expected_correct_coordinates_call_three = [-4.14060652256012, 50.37544283566208, 2]

    def setUp(self):
        self.api = MockGpsApi.app.test_client()

    def testThatIncorrectTrackGivesCorrectResponse(self):
        print(self._testMethodName)
        incorrect_route_id = 5
        response = self.api.get('/gps/' + str(incorrect_route_id))
        response_json = json.loads(response.data)
        message = response_json[0]['content']
        self.assertEqual(self.expected_no_track_message + str(incorrect_route_id), message)

    def testThatCorrectTrackGivesCorrectResponse(self):
        print(self._testMethodName)
        correct_route_id = 2
        response = self.api.get('/gps/' + str(correct_route_id))
        response_json = json.loads(response.data)
        coord1 = response_json[0]['coords'][0]
        coord2 = response_json[0]['coords'][1]
        count = response_json[0]['coord count']
        self.assertEqual(coord1, self.expected_correct_coordinates_call_one[0])
        self.assertEqual(coord2, self.expected_correct_coordinates_call_one[1])
        self.assertEqual(count, self.expected_correct_coordinates_call_one[2])

        response = self.api.get('/gps/' + str(correct_route_id))
        response_json = json.loads(response.data)
        coord1 = response_json[0]['coords'][0]
        coord2 = response_json[0]['coords'][1]
        count = response_json[0]['coord count']
        self.assertEqual(coord1, self.expected_correct_coordinates_call_two[0])
        self.assertEqual(coord2, self.expected_correct_coordinates_call_two[1])
        self.assertEqual(count, self.expected_correct_coordinates_call_two[2])

        response = self.api.get('/gps/' + str(correct_route_id))
        response_json = json.loads(response.data)
        coord1 = response_json[0]['coords'][0]
        coord2 = response_json[0]['coords'][1]
        count = response_json[0]['coord count']
        self.assertEqual(coord1, self.expected_correct_coordinates_call_three[0])
        self.assertEqual(coord2, self.expected_correct_coordinates_call_three[1])
        self.assertEqual(count, self.expected_correct_coordinates_call_three[2])


if __name__ == "__main__":
    main()