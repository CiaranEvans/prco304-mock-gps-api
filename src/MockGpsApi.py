import json
import os
from flask import Flask, jsonify
from shapely.geometry import shape

app = Flask(__name__)

SITE_ROOT = os.path.realpath(os.path.dirname(__file__))

JSON_1_URL = os.path.join(SITE_ROOT, 'geojson', 'geojson1.json')
counter_1 = 0
route_1 = shape(json.loads(open(JSON_1_URL).read()))
JSON_2_URL = os.path.join(SITE_ROOT, 'geojson', 'geojson2.json')
counter_2 = 0
route_2 = shape(json.loads(open(JSON_2_URL).read()))
JSON_3_URL = os.path.join(SITE_ROOT, 'geojson', 'geojson3.json')
counter_3 = 0
route_3 = shape(json.loads(open(JSON_3_URL).read()))


@app.route('/gps/<int:route_id>')
def get_gps_1_pos(route_id):
    global counter_1, counter_2, counter_3
    return_json = jsonify([{'content': 'No route available for: ' + str(route_id)}])
    if route_id == 1:
        return_json = jsonify([{'coords': route_1.coords[counter_1],
                                'coord count': counter_1}])
        if counter_1 == (len(route_1.coords) - 1):
            counter_1 = 0
        else:
            counter_1 = counter_1 + 1
        return return_json, 201
    if route_id == 2:
        return_json = jsonify([{'coords': route_2.coords[counter_2],
                                'coord count': counter_2}])
        if counter_2 == (len(route_2.coords) - 1):
            counter_2 = 0
        else:
            counter_2 = counter_2 + 1
        return return_json, 201
    if route_id == 3:
        return_json = jsonify([{'coords': route_3.coords[counter_3],
                                'coord count': counter_3}])
        if counter_3 == (len(route_3.coords) - 1):
            counter_3 = 0
        else:
            counter_3 = counter_3 + 1
        return return_json, 201
    return return_json, 400


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000, use_reloader=False)